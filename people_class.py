
class Person:
    """This is the parent class from which other classes will inherit"""

    def __init__(self, iD, personId, name, age, gender, path):
        """
        Arguments:  iD {int}              -- Identification obtained after analyzing 
                                             the characteristics of the person's face
                    personId {int}        -- Own identification of the person
                    name {str}            -- Name of the person
                    age {int}             -- Age of the peron
                    gender {str}          -- Gender of the person
                    path {str}            -- Path of the person's image
        """
        self.iD = iD
        self.personId = personId
        self.name = name
        self.age = age
        self.gender = gender
        self.path = path

class Cashier(Person):
    """This class inherits from the "Person" class and 
       from here the cashier object is generated"""

    def __init__(self, iD, personId, name, age, gender, path, workingHours, salary):
        """
        Constructor of the cashier object
        
        Arguments:  iD {int}              -- Identification obtained after analyzing 
                                             the characteristics of the person's face
                    personId {int}        -- Own identification of the person
                    name {str}            -- Name of the person
                    age {int}             -- Age of the peron
                    gender {str}          -- Gender of the person
                    path {str}            -- Path of the person's image
                    workingHours {int}    -- Cashier hours worked
                    salary {int}          -- Cashier's salary
        """
        Person.__init__(self, iD, personId, name, age, gender, path)
        self.workingHours = workingHours
        self.salary = salary

    def __str__(self):
        return """Id = {}, personId = {}, name = {}, age = {}, gender = {}, path = {}, workingHourse = {}, salary = {}""".format(
            self.iD, self.personId, self.name, self.age, self.gender, self.path, 
            self.workingHours, self.salary
        )

class Client(Person):
    """This class inherits from the "Person" class and 
       from here the client object is generated"""

    def __init__(self, iD, personId, name, age, gender, path, phone, address):
        """
        Constructor of the client object

        Argumnts:   iD {int}              -- Identification obtained after analyzing 
                                             the characteristics of the person's face
                    personId {int}        -- Own identification of the person
                    name {str}            -- Name of the person
                    age {int}             -- Age of the peron
                    gender {str}          -- Gender of the person
                    path {str}            -- Path of the person's image
                    phone {int}           -- Person's phone number
                    address {str}         -- Person's email addres
        """
        Person.__init__(self, iD, personId, name, age, gender, path)
        self.phone = phone
        self.address = address

    def __str__(self):
        return """Id = {}, personId = {}, name = {}, age = {}, gender = {}, path = {}, phone = {}, address = {}""".format(
            self.iD, self.personId, self.name, self.age, self.gender, self.path, 
            self.phone, self.address
        )

class Boss(Person):
    """This class inherits from the "Person" class and 
       from here the boss object is generated"""
       
    def __init__(self, iD, personId, name, age, gender, path, yearsasBoss, degree):
        """
        Constructor of the boss object

        Arguments:  iD {int}              -- Identification obtained after analyzing 
                                             the characteristics of the person's face
                    personId {int}        -- Own identification of the person
                    name {str}            -- Name of the person
                    age {int}             -- Age of the peron
                    gender {str}          -- Gender of the person
                    path {str}            -- Path of the person's image
                    yearsasBoss {int}     -- Years that the person has been a boss
                    degree {bool}         -- To check if the boss has a bachelor's degree
        """
        Person.__init__(self, iD, personId, name, age, gender, path)
        self.yearsasBoss = yearsasBoss
        self.degree = degree

    def __str__(self):
        return """Id = {}, personId = {}, name = {}, age = {}, gender = {}, path = {}, yearsasBoss = {}, degree = {}""".format(
            self.iD, self.personId, self.name, self.age, self.gender, self.path, 
            self.yearsasBoss, self.degree
        )
class Product:
    """This class allows to create an object of type product
    """
    def __init__(self, productId, description,  price): #Constructor of the Product object
        
        """
        Arguments:
        productId {int}        -- the id of the product
        description {str}      -- the description of the product
        price {int}            -- the price of the product
        """
        self.productId = productId
        self.description = description
        self.price = price

    def __str__(self):
        return "Product id: {}, Description: {}, Price: {}".format(
            self.productId, self.description, self.price)
    

class Sale:
    """ This class allows to create an object of type Sale"""
    def __init__(self, date, cashierId, clientId, invoiceId, amount, cashier_path,
     client_path, productsIds, price): #Constructor of the Sale object
        """
        Arguments:
        date {int} -- the date of the sale
        cashierId {int} --the cashierId of the cashier
        clientId {int} --the clientId of the client
        invoiceId {int} -- the invoiceId of the sale
        amount {int} -- the amount given by the client to buy the product
        cashier_path {str} -- the path of the cashier
        client_path {str} -- the path of the client
        productsIds {int} -- the id of the products purchased
        price {int} -- the price of the purchased product
        """
        self.date = date
        self.cashierId = cashierId
        self.clientId = clientId
        self.invoiceId = invoiceId
        self.amount = amount
        self.cashier_path = cashier_path
        self.client_path = client_path
        self.productsIds = productsIds
        self.price = price
    
    def __str__(self):
        return """CashierId: {}, ClientId: {}, InvoiceId: {}, Amount: {}, Cashier_path: {}, Client_path: {}, ProductsIds: {}, Price: {}""".format(
            self.cashierId, self.clientId, self.invoiceId, self.amount, 
            self.cashier_path, self.client_path, self.productsIds, self.price)

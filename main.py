import sys
import requests
import json

import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont
from os import system
import os

from people_class import *
import pickle
import time

from random import randint
from sales import *
from tabulate import tabulate

subscription_key = None

SUBSCRIPTION_KEY = '6f05341484464f6b97557ccdd0b45619' 
BASE_URL = 'https://etapa2proyectottaller.cognitiveservices.azure.com//face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)

def emotions(picture):
    """Get a person's emotions
    Argument:
        picture {strg}      -- receive the path of the photo of the person you want to get the emotions
    Return:
        analysis {lista}    -- returns the list with the face dictionaries

    """
    #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
    image_path = picture
    #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
                             BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()
    return analysis


def create_group(group_id, group_name):
    """Create a new group of people.
    Arguments:
        group_id {int}          -- is the group id
        group_name {str}        -- is the name of the group

    You only have to charge it the first time
    """
    CF.person_group.create(group_id, group_name)
    print("Group created")


def create_person(name, profession, picture, group_id):
    """Create a person in a group
    Argumentos:
        name {str}              -- is the name of the person
        profession {str}        -- is the person's profession
        picture     {str}       -- is the path of the person's photo
        group_id    {str}       -- is the group to which you want to add the person
    """
    response = CF.person.create(group_id, name, profession)
    #In response comes the person_id of the person that has been created
    # Get person_id from response
    person_id = response['personId']
    #Add a photo to the person that has been created
    CF.person.add_face(picture, group_id, person_id)
    
    #Re-train the model because a photo was added to a person
    CF.person_group.train(group_id)
    #Get group status
    response = CF.person_group.get_status(group_id)
    status = response['status']
    return person_id

         
def print_people(group_id):
    """Print the list of people who belong to a group
    Arguments:
        group_id {str}      -- is the id of the group that you want to print your people
    """
    #Print the list of people in the group
    print(CF.person.lists(group_id))


def face_rectangle(path): #Exercise k
    """ This function displays a rectangle on the face of all persons in the image.
    Arguments:
    face_list {list} -- list with the data of the faces of the people in the photo.
    path {str} -- Address where the image provided by the user is located
    """
    face_list = emotions(path)
    faces = []
    for face in face_list:
        faceRectangle = face["faceRectangle"]
        faces.append(faceRectangle)

    image = Image.open(path)
    draw = ImageDraw.Draw(image)
    for fr in faces:
        top = fr['top']
        left = fr['left']
        width= fr['width']
        height = fr['height']
        draw.rectangle((left,top, left+width, top+height), outline='blue', width=5)
    image.show()


def recognize_person(picture, group_id):
    """Recognize people from a photo
    Arguments:
        picture {str}       -- is the path of the photo of the person you want to recognize
        group_id {str}      -- is the id of the group in which you want to search for the person
    """
    print(picture, " ", group_id)
    response = CF.face.detect(picture)
    face_ids = [d['faceId'] for d in response]
    print(response)    
    identified_faces = CF.face.identify(face_ids, group_id)
    personas = identified_faces[0]
    print(personas)
    candidates_list = personas['candidates']
    candidates = candidates_list[0]
    #print(candidates)
    person = candidates['personId']
    #print(persona)
    person_data = CF.person.get(group_id, person)
    #print(persona_info)
    person_name = person_data['name']
    #print(person_name)
    
    #Detectar si esta foto pertenece a alguien de la familia
    response = CF.face.detect(picture)
    #print(response)
    dic = response[0]
    #print(dic)
    faceRectangle = dic['faceRectangle']
    #print(faceRectangle)
    width = faceRectangle['width']
    top = faceRectangle['top']
    height = faceRectangle['height']
    left = faceRectangle['left']
    image=Image.open(picture)
    draw = ImageDraw.Draw(image)
    draw.rectangle((left,top,left + width,top+height), outline='red')
    font = ImageFont.truetype('Arial_Unicode.ttf', 50)
    draw.text((50, 50), person_name, font=font,  fill="white")
    image.show()


def create_product():
    """Create a product and add it to a file
    """
    system("cls")
    productId = input("\n Enter the id of the product to create: ")
    description = input("\n Enter de name of the product: ")
    price = int(input("\n Enter the price of the product: "))
    product1 = Product(productId, description, price)
    with open("products.bin", "ab") as f:
        pickle.dump(product1, f, pickle.HIGHEST_PROTOCOL)
    print(f"\n The product: {product1.description} \n have been added to the list of products.")


def read_file(file):
    """This function reads the files where data such as cashiers, customers and among others are stored.
    Arguments:
    file {str}   -- The name of the file to read
    """
    f = open(file, "rb")
    f.seek(0)
    flag = 0
    products_list = []
    print()
    while flag == 0:
        try:
            if file == "cashiers.bin":
                e = pickle.load(f)
                print(e)                                   
                face_rectangle(e.path) # ¿Qué foto se debe mostrar?
            elif file == "clients.bin":
                e = pickle.load(f) 
                print(e)
                face_rectangle(e.path)
            elif file == "bosses.bin":
                e = pickle.load(f)
                print(e)
                face_rectangle(e.path)  
            elif file == "products.bin":
                e = pickle.load(f)
                products_list.append((e.productId, e.description, e.price))
            elif file == "sales.bin":
                e = pickle.load(f)
                print(e.date)
                print(e, "\n") 
        except:
            flag = 1
    f.close
    return products_list


def obtain_price(product_Id):
    """This function is responsible for accessing the file with the products and obtaining 
    the price of the product with the ID provided
    Arguments:
    product_Id {str} --receive the id of the product that we want to obtain the price

    Returns:
    price {int} --return the price of the product that we wanted to know
    """
    f = open("products.bin", "rb")
    f.seek(0)
    flag = 0
    while flag ==0:
        try:
            e = pickle.load(f)
            if e.productId == product_Id:
                price = e.price
                return price
        except:
            flag =1
    f.close


def read_cashiers_clients(file):
    f = open(file, "rb")
    f.seek(0)
    flag = 0
    c = 1
    while flag == 0:
        try:
            e = pickle.load(f)
            print(c,":", "Name:", e.name,",", "ID:", e.iD)
            c += 1
        except:
            flag = 1
    f.close


def make_sale():
    system("cls")
    """Function to function that allows you to create an instance of a sale and save it in a file"""
    print("Greetings dear person, \nyou are about to make a sale in one of the best supermarkets in the world")
    date_time = time.asctime()
    print("\nThe cashiers available are: ")
    read_cashiers_clients("cashiers.bin")
    cashierId = input("\nEnter the id of the cashier to choose: ")
    print("\nThe clients available are: ")
    read_cashiers_clients("clients.bin")
    clientId = input("\nEnter de id of the client: ")
    invoiceId = str(randint(10000,99999))
    cashier_path = input("\nEnter the path of the cashier: ")
    client_path = input("\nEnter the path of the client: ")
    system("cls")
    print("\nWe currently have these fabulous products:")
    products = read_file("products.bin")
    headers = ["ID", "Description", "Price"]
    table = tabulate(products, headers, tablefmt = "fancy_grid")
    print(table)
    final_price = 0
    product_list = []
    while True:
        productsIds = input("\nPlease indicate the id of the product(s) you want to buy: ")
        price = obtain_price(productsIds)
        final_price += price
        product_list.append(productsIds)
        petition = input("\nDo you want to purchase another product?  Write: Yes/No: ").lower()
        if petition == "no":
            break

    amount = int(input("\nEnter the amount with which you want to pay: "))
    os.system("caja-registradora.mp3")
    sale = Sale(date_time,cashierId,clientId, invoiceId,amount, cashier_path, client_path, product_list,final_price )
    with open("sales.bin", "ab") as f:
        pickle.dump(sale, f, pickle.HIGHEST_PROTOCOL)
        print("\nSuccessful sale")
    

def get_emotions(path):
    picture = emotions(path)
    for i in picture:
        faceAttributes = i["faceAttributes"]
        emotions1 = faceAttributes["emotion"]
    return emotions1


def cashiers_sales():
    print("These are the cashiers:")
    read_cashiers_clients("cashiers.bin")
    cashier_id = input("\nEnter the id of the cashier that you want to see his sales: ")

    sales_list = []
    f = open("sales.bin", "rb")
    f.seek(0)
    flag = 0
    while flag == 0:
        try:
            e = pickle.load(f)
            if e.cashierId == cashier_id:
                sales_list.append([e])
        except:
            flag =1
    f.close

    print()
    for i in sales_list:
        f = open("cashiers.bin", "rb")
        f.seek(0)
        flag = 0
        while flag == 0:
            try:
                e = pickle.load(f)
                if e.iD == int(cashier_id):
                    print("Cashier:", e)
            except:
                flag = 1
        f.close

        clientId = int(i[0].clientId)
        f = open("clients.bin", "rb")
        f.seek(0)
        flag = 0
        while flag == 0:
            try:
                e = pickle.load(f)
                if e.iD == clientId:
                    print("Client:", e)
            except:
                flag = 1
        f.close

        cashier_path = i[0].cashier_path
        cashier_emotions = get_emotions(cashier_path)
        client_path = i[0].client_path
        client_emotions = get_emotions(client_path)

        print("\n", i[0].date,":", i[0])
        print("The cashiers emotions are:", cashier_emotions)
        print("The client emotions are: ", client_emotions)

        image = Image.open(cashier_path)
        draw = ImageDraw.Draw(image)
        image.show()

        image = Image.open(client_path)
        draw = ImageDraw.Draw(image)
        image.show()
        print()


def create_group1():
    system("cls")
    group_id = int(input("Enter the group id: "))
    group_name = input("Enter the name of the group: ")
    create_group(group_id, group_name)

    print("\nGroup successfully created")


def consult_person():
    picture = input("Dígite el path de la imagen: ")
    group_id = input("Dígite el id del grupo: ")
    recognize_person(picture, group_id)
    input("\nPress enter to continue...")
    

def create_person_group():
    system("cls")

    name = input("Enter the name: ")
    picture = input("Enter the image path: ")
    

    print("""\nWich person would you like to add?
    1. Cashier
    2. Client
    3. Boss""")

    option = int(input("Choose an option: "))

    while True:
        if option == 1:
            system("cls")
            iD = int(input("Enter the id of the cashier: "))
            workingHours = int(input("Enter hours worked: "))
            salary = int(input("Enter the salary of the cashier: "))
            profession = "cashier"

            person_id = create_person(name, profession, picture, 101)
            face_list = emotions(picture)
            dic = face_list[0]
            faceAttributes = dic["faceAttributes"]
            age = faceAttributes["age"]
            gender = faceAttributes["gender"]
            cashier = Cashier(iD, person_id, name, age, gender, picture, workingHours, salary)

            with open("cashiers.bin", "ab") as f:
                pickle.dump(cashier, f, pickle.HIGHEST_PROTOCOL)

            print("\nThe cashier has been added correctly")
            break
        elif option == 2:
            system("cls")
            iD = int(input("Enter the id of the client: "))
            phone = int(input("Enter the phone of the client: "))
            address = input("Enter the client's email address: ")
            profession = "client"

            person_id = create_person(name, profession, picture, 102)
            face_list = emotions(picture)
            dic = face_list[0]
            faceAttributes = dic["faceAttributes"]
            age = faceAttributes["age"]
            gender = faceAttributes["gender"]
            client = Client(iD, person_id, name, age, gender, picture, phone, address)

            with open("clients.bin", "ab") as f:
                pickle.dump(client, f, pickle.HIGHEST_PROTOCOL)

            print("\nThe client has been added correctly")
            break
        elif option == 3:
            system("cls")
            iD = int(input("Enter the id of the boss: "))
            yearsasBoss = int(input("Enter the years as boss: "))
            profession = "boss"

            while True:
                degree = input("Does the boss have a bachelor's degree? [Yes/No] ").lower()

                if degree == "yes":
                    degree = True
                    break
                elif degree == "no":
                    degree = False
                    break
                else:
                    print("\nThe option enter is invalid")
                    input("Press enter to try again...")
                    system("cls")
            person_id = create_person(name, profession, picture, 103)
            face_list = emotions(picture)
            dic = face_list[0]
            faceAttributes = dic["faceAttributes"]
            age = faceAttributes["age"]
            gender = faceAttributes["gender"]
            boss = Boss(iD, person_id, name, age, gender, picture, yearsasBoss, degree)

            with open("bosses.bin", "ab") as f:
                pickle.dump(boss, f, pickle.HIGHEST_PROTOCOL)

            print("\nThe boss has been added correctly")
            input("Please enter to continue...")
            break
        else:
            print("\nThe option enter is invalid")
            input("Press enter to try again...")


if __name__ == "__main__":
    while True:
        system("cls")
        try:
            print("""What do you want to to?
        1. Create things on the supermarket
        2. Make Consult
        3. Exit the program""")

            option = int(input("\nChoose an option: "))

            if option == 1:
                while True:
                    system("cls")
                    print("""Choose the task you want to perform
            1. Create group
            2. Create a person in a group
            3. Create a product
            4. Make a sale
            5. Return to the main menu
                """)

                    case = int(input("Choose an option: "))
                    if case == 1:              
                        create_group1()
                    elif case == 2:             
                        create_person_group()
                    elif case == 3:
                        create_product()
                    elif case == 4:
                        make_sale()
                    elif case == 5:
                        break
                    else:
                        print("\nThe chosen option is not valid")
                        input("Pres enter to try again...")
                    input("Press enter to continue...")
            elif option == 2:
                while True:
                    system("cls")
                    print("""Choose the task you want to perform
            1. See if a person exists
            2. See files
            3. View a cashier's sales
            4. Return to the main menu""")

                    case = int(input("Choose an option: "))
                    if case == 1:
                        try:
                            print("""
    ID Cashiers group: 101
    ID Clients group: 102
    ID Bosses group: 103\n""")
                            group_id = input("Enter de group id: ")
                            picture = input("Enter the path of the image: ")
                            recognize_person(picture, group_id)
                            input("\nPress enter to continue...")
                        except:
                            print("\nThe person dosen't exist in the group")
                            input("Press enter to continue")
                    elif case == 2:
                        while True:
                            system("cls")
                            print("""What do you want to see?
                1. Cashiers
                2. Clients
                3. Bosses
                4. Products
                5. Sales
                6. Exit to main menu""")

                            option = int(input("Choose an option: "))

                            try:
                                if option == 1:
                                    read_file("cashiers.bin")
                                elif option == 2:
                                    read_file("clients.bin")
                                elif option == 3:
                                    read_file("bosses.bin")
                                elif option == 4:
                                    system("cls")
                                    products = read_file("products.bin")
                                    headers = ["ID", "Description", "Price"]
                                    table = tabulate(products, headers, tablefmt = "fancy_grid")
                                    print("The products available are: ")
                                    print(table)
                                elif option == 5:
                                    read_file("sales.bin")
                                elif option == 6:
                                    break
                                input("\nPress enter to continue...")
                            except:
                                print("\nThe file does not exists")
                                input("Press enter to try again...")
                    elif case == 3:
                        cashiers_sales()
                        input("\nPress enter to continue...")
                    elif case == 4:
                        break
                    else:
                        print("\nThe chosen option is invalid")
                        input("Press enter to try again...")
            elif option == 3:
                break
            else:
                print("\nThe chosen option is not valid")
                input("Pres enter to try again...")
        except:
            print("\nThe option entered must be a digit")
            input("Press enter to try again...")